package com.srikanth.oncode.testing;

import java.util.List;

public class EvenNumbersStream {
	
	public static  List<Integer> EvenNumbers(List<Integer> list){
		
		list.stream()
			.forEach(System.out::println);
			
		List<Integer> list2=list.stream()
			.filter(x->x%2==0)
			.toList();
		return list2;
	}
}
