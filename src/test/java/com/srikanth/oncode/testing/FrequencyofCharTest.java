package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;

import org.junit.jupiter.api.Test;

class FrequencyofCharTest {

	@Test
	void test() {
		Map<Character,Integer> map=FrequencyofChar.frequency("abccd");
		for(Character e:map.keySet()) {
			System.out.println(map.get(e));
		}
		assertEquals(1,map.get('a'));
		assertEquals(1,map.get('b'));
		assertEquals(2,map.get('c'));
		assertEquals(1,map.get('d'));
	}

}
