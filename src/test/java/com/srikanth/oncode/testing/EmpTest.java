package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EmpTest {

	@Test
	void testforClerk() {
		Clerk clerk=new Clerk();
		clerk.getDisplay();
		assertEquals(1150000,clerk.getNetSalary());
	}
	@Test
	void testforDriveManager() {
		DriveManager driveManager=new DriveManager();
		driveManager.getDisplay();
		assertEquals(1140000,driveManager.getNetSalary());
	}

}
