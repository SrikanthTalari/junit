package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SquaresTest {

	@Test
	void testforGenerate() {
		int[] result=Squares.generateSqure(new int[]{1,2,3,4,5,6,7,8,9,10});
		assertNotNull(result);
		assertEquals(10, result.length);
		
	}
	@Test
	void testforBoundery() {
		int[] result=Squares.generateSqure(new int[]{1,2,3,4,5,6,7,8,9,11});
		assertNotNull(result);
		assertThrows(UserDefinedException.class,()->Squares.checkBoundery(result));
	}

}
