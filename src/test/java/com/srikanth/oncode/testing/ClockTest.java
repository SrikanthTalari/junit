package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ClockTest {

	@Test
	void test1() {
		Clock clock=new Clock(12, 23, 45);
		clock.setTime("AM");
		boolean result=clock.validity();
		assertEquals(true, result);
	}
	@Test
	void test2() {
		Clock clock=new Clock(25, 23, 45);
		clock.setTime("PM");
		boolean result=clock.validity();
		assertEquals(false, result);
	}
	

}
