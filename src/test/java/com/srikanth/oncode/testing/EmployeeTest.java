package com.srikanth.oncode.testing;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmployeeTest {

	Employee emp;
	@BeforeEach
	public void objInitilizaton() {
		emp=new Employee();
	}
	@Test
	public void test1() {
		assertEquals(1,emp.sCount);
		assertEquals(1,emp.nsCount);
	}
	
	@Test
	public void test2() {
		assertEquals(1,emp.nsCount);
		assertEquals(2,emp.sCount);
	}
	

}
