package com.srikanth.oncode.testing;

import java.util.Comparator;
import java.util.List;

public class EmployeeProblem{
	private int id;
	private String name;
	private String dept;
	public EmployeeProblem(int id, String name, String dept) {
		super();
		this.id = id;
		this.name = name;
		this.dept = dept;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}
	

	public static List<EmployeeProblem> sortEmp(List<EmployeeProblem> list) {
		
		List<EmployeeProblem> result=list.stream()
			.sorted((p1,p2)->p1.getName().compareTo(p2.getName()))
			.toList();
		return result;
	}
	public static boolean isSorted(List<EmployeeProblem> list) {
		boolean flag=false;
		for(int i=0;i<list.size()-1;i++) {
			if(list.get(i).getName().compareTo(list.get(i+1).getName())<0) {
				flag=true;
			}
		}
		return flag;
	}
	
}
