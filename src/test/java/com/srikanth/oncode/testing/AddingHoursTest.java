package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

class AddingHoursTest {

	@Test
	void test1() {
		LocalDateTime currentdatetime=LocalDateTime.now();
		LocalDateTime updateddatetime=currentdatetime.plusHours(10);
		assertEquals(updateddatetime.getHour(), AddingHours.addingHours().getHour());
		
	}
	@Test
	void test2() {
		LocalDateTime currentdatetime=LocalDateTime.now();
		LocalDateTime updateddatetime=currentdatetime.plusHours(10);
		assertEquals(updateddatetime.getHour(), AddingHours.addingHours().getHour());
		
	}

}
