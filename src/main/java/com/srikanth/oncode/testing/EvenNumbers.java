package com.srikanth.oncode.testing;

import java.util.ArrayList;
import java.util.List;

public class EvenNumbers {
	public static List<Integer> A1;
	public static List<Integer> A2;
	public static List<Integer> saveEvenNumbers(int N){
		A1=new ArrayList<Integer>();
		for(int i=0;i<=N;i+=2) {
			A1.add(i);
		}
		return A1;
	}
	public static List<Integer> printEvenNumbers(){
		A2=new ArrayList<>();
		for(int i=0;i<A1.size();i++) {
			A2.add(A1.get(i)*2);
		}
		return A2;
	}
}
