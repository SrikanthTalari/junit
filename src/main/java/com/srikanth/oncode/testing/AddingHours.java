package com.srikanth.oncode.testing;

import java.time.LocalDateTime;

public class AddingHours {
	private static LocalDateTime currentdatetime;
	public static LocalDateTime addingHours() {
		LocalDateTime currentdatetime=LocalDateTime.now();
		LocalDateTime updateddatetime=currentdatetime.plusHours(10);
		return updateddatetime;
	}
}
