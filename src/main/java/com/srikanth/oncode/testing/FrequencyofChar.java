package com.srikanth.oncode.testing;

import java.util.HashMap;
import java.util.Map;

public class FrequencyofChar {

	public static Map<Character,Integer> frequency(String input){
		 Map<Character,Integer> map=new HashMap<>();
		 for(int i=0;i<input.length();i++) {
			 Integer count=map.get(input.charAt(i));
			 if(count==null) {
				 map.put(input.charAt(i),1);
			 }
			 else {
				 map.put(input.charAt(i),++count);
			 }
		 }
		 return map;
	}
}
