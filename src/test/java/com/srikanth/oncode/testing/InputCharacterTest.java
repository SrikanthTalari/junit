package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Scanner;

import org.junit.jupiter.api.Test;

import com.srikanth.oncode.testing.InputCharacter;

class InputCharacterTest {

	@Test
	public void testUpperCase() {
		Scanner scan=new Scanner(System.in);
		String input=scan.next();
		assertNotNull(input);
		assertEquals(true, InputCharacter.isUpper(input));
	}
	
	@Test
	public void testLowerCase() {
		Scanner scan=new Scanner(System.in);
		String input=scan.next();
		assertNotNull(input);
		assertEquals(true,InputCharacter.isLower(input));
	}
	@Test
	public void testNumberCase() {
		Scanner scan=new Scanner(System.in);
		String input=scan.next();
		assertNotNull(input);
		assertEquals(true,InputCharacter.isNumber(input));
	}

}
