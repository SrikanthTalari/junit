package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.Period;

import org.junit.jupiter.api.Test;

class DataDiffTest {

	@Test
	void test() {
		
		Period p=DataDiff.diff(LocalDate.of(2007, 11, 03),LocalDate.of(2007, 12, 03));
		assertEquals(0, p.getYears());
		assertEquals(1, p.getMonths());
		assertEquals(0, p.getDays());
	}

}
