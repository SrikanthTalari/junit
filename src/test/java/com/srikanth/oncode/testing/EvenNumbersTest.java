package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class EvenNumbersTest {

	
	@Test
	void test2() {
		EvenNumbers.printEvenNumbers();
		assertNotNull(EvenNumbers.A2);
		for(int i=0;i<EvenNumbers.A1.size();i++) {
			assertEquals(EvenNumbers.A1.get(i), EvenNumbers.A2.get(i)/2);
		}
	}
	
	@Test
	void test1() {
		EvenNumbers.saveEvenNumbers(10);
		assertNotNull(EvenNumbers.A1);
		for(int i=0;i<EvenNumbers.A1.size();i++) {
			assertEquals(0, EvenNumbers.A1.get(i)%2);
		}
		
	}
	
	

	
	

}
