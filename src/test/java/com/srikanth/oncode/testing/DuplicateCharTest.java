package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DuplicateCharTest {
	private int n;
	@Test
	void test1() {
		n=DuplicateChar.duplicateCharacteres("aaa");
		assertEquals(2,n);
	}
	@Test
	void test2() {
		n=DuplicateChar.duplicateCharacteres("axx");
		assertEquals(1, 1);
	}

}
