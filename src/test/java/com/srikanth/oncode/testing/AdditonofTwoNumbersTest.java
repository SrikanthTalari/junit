package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AdditonofTwoNumbersTest {
	private static int m1(int n1,int n2,AdditonofTwoNumbers numbers) {
		return numbers.addition(n1,n2);
	}
	@Test
	void test1() {
		int result=AdditonofTwoNumbersTest.m1(10, 20, (n1,n2)->n1+n2);
		
		assertEquals(30,result);
	}
	@Test
	void test2() {
		int result=AdditonofTwoNumbersTest.m1(15, 20, (n1,n2)->n1+n2);
		assertEquals(35,result);
	}

}
