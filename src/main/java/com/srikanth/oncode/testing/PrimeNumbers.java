package com.srikanth.oncode.testing;

public class PrimeNumbers {

	public static void primeNums(int[] arr) {
		for(int i=0;i<arr.length;i++) {
			int count=0;
			for(int j=2;j<=arr[i];j++) {
				if(arr[i]%j==0) {
					count++;
				}
			}
			if(count==1) {
				System.out.println(arr[i]);
			}
		}
	}
	
	public static void main(String[] args) {
		 int[] arr=new int[args.length];
		for(int i=0;i<args.length;i++) {
			arr[i]=Integer.valueOf(args[i]);
		}
		primeNums(arr);
	}

}
