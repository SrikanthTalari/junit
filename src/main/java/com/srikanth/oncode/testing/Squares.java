package com.srikanth.oncode.testing;

public class Squares {
	public static int[] generateSqure(int[] nums) {
		int[] squres=new int[nums.length];
		for(int i=0;i<nums.length;i++) {
			squres[i]=nums[i]*nums[i];
		}
		return squres;
	}
	public static void checkBoundery(int[] squres) throws UserDefinedException {
		for(int i=0;i<squres.length;i++) {
			if(squres[i]>100) {
				throw new UserDefinedException(squres[i]+" is out of boundery");
			}
		}
	}
}
class UserDefinedException extends Exception{
	private String msg;
	public UserDefinedException(String msg) {
		this.msg=msg;
	}
}
