package com.srikanth.oncode.testing;

public class Clock {
	private int hours;
	private int minutes;
	private int seconds;
	private String time;
	public Clock(int hours, int minutes, int seconds) {
		super();
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	public boolean validity() {
		if((this.hours<=12 && this.hours>=00)&&(this.minutes<=60 && this.minutes>=0)&&(this.seconds>=00 && this.seconds<=60)){
			if(this.time.equals("AM")||this.time.equals("PM")) {
				return true;
			}
		}
		return false;
	}
}
