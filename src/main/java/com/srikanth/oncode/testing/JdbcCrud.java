package com.srikanth.oncode.testing;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcCrud {
	final static String jdbc_driver="com.mysql.cj.jdbc.Driver";
	final static String jdbc_url="jdbc:mysql://localhost:3306/users_database";
	final static String username="root";
	final static String password="root";
	
	public static boolean jdbcRead() {
		try {
			Class.forName(jdbc_driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection conn=null;
		Statement stmt=null;
		boolean flag=false;
		try {
			 conn=DriverManager.getConnection(jdbc_url,username,password);
			 stmt=conn.createStatement();
			String query="select * from employees";
			ResultSet result=stmt.executeQuery(query);
			while(result.next()) {
				System.out.println(result.getString("id")+" : "+result.getString("first_name")+" : "+result.getString("last_name")+" : "+result.getString("email_address"));
				if(flag==false) {
					flag=!flag;
				}
			}
			
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				conn.close();
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
		}
		return flag;
	}
	
	public static boolean jdbcInsert() {
		boolean flag=false;
		int insertHappend=0;
		try {
			Class.forName(jdbc_driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection conn=null;
		PreparedStatement stmt=null;
		try {
			 conn=DriverManager.getConnection(jdbc_url,username,password);
			 String query="insert into employees values(?,?,?,?)";
			 stmt=conn.prepareStatement(query);
			 stmt.setInt(1, 3);
			 stmt.setString(2, "kalpana@gmail.com");
			 stmt.setString(3,"kalpana");
			 stmt.setString(4, "vanga");
			 insertHappend=stmt.executeUpdate();
			System.out.println("Data is inserted"+insertHappend);
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				conn.close();
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
		}
		if(insertHappend==1) {
			flag=!flag;
		}
		
		return flag;
	}
	
	public static boolean jdbcDelete() {
		boolean flag=false;
		int deleteHappend=0;
		try {
			Class.forName(jdbc_driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection conn=null;
		PreparedStatement stmt=null;
		try {
			 conn=DriverManager.getConnection(jdbc_url,username,password);
			 String query="delete from employees where id=?";
			 stmt=conn.prepareStatement(query);
			 stmt.setInt(1, 3);
			
			 deleteHappend=stmt.executeUpdate();
			 System.out.println("data id deleted "+deleteHappend);
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				conn.close();
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
		}
		if(deleteHappend==1) {
			flag=!flag;
		}
		return flag;
	}
	
	public static boolean jdbcUpdate() {
		boolean flag=false;
		int updateHappend=0;
		try {
			Class.forName(jdbc_driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection conn=null;
		PreparedStatement stmt=null;
		try {
			 conn=DriverManager.getConnection(jdbc_url,username,password);
			 String query="insert into employees values(?,?,?,?)";
			 stmt=conn.prepareStatement(query);
			 stmt.setInt(1, 2);
			 stmt.setString(2, "raman@gmail.com");
			 stmt.setString(3,"raman");
			 stmt.setString(4, "gopal");
			 updateHappend=stmt.executeUpdate();
			System.out.println("Data is inserted");
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				conn.close();
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
		}
		if(updateHappend==1) {
			flag=!flag;
		}
		return flag;
	}
}
