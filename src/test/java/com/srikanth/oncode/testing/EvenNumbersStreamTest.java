package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class EvenNumbersStreamTest {

	@Test
	void test() {
		List<Integer> list=Arrays.asList(1,2,3,4);
		List<Integer> result=EvenNumbersStream.EvenNumbers(list);
		for(int i=0;i<result.size();i++) {
			assertEquals(0,result.get(i)%2);
		}
	}

}
