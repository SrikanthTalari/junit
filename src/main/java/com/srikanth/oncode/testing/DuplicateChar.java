package com.srikanth.oncode.testing;

import java.util.HashSet;
import java.util.Set;

public class DuplicateChar {
	public static int duplicateCharacteres(String input) {
		int count=0;
		Set<Character> set=new HashSet<>();
		for(int i=0;i<input.length();i++) {
			if(set.contains(input.charAt(i))) {
				count++;
			}
			set.add(input.charAt(i));
		}
		return count;
	}
}
