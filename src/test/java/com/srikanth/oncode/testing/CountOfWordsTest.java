package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CountOfWordsTest {

	@Test
	void testforTwoWords() {
		CountOfWords.main(new String[] {"abc","xyz"});
		assertEquals(2, CountOfWords.count);
	}
	@Test
	void testforThreeWords() {
		CountOfWords.main(new String[] {"abc","xyz","mkl"});
		assertEquals(3, CountOfWords.count);
	}

}
