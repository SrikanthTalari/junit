package com.srikanth.oncode.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class EmployeeProblemTest {

	@Test
	void test() {
		EmployeeProblem e1=new EmployeeProblem(1, "suresh", "cse");
		EmployeeProblem e2=new EmployeeProblem(2, "ramesh","ece");
		EmployeeProblem e3=new EmployeeProblem(3, "sujan", "mech");
		List<EmployeeProblem> list=new ArrayList<>();
		list.add(e1);
		list.add(e2);
		list.add(e3);
		List<EmployeeProblem> result=EmployeeProblem.sortEmp(list);
		assertNotNull(result);
		assertEquals(true, EmployeeProblem.isSorted(result));
	}

}
