package com.srikanth.oncode.testing;

public abstract class Emp {
	private String name="sujan";
	private String id="12023";
	private double salary=12000;
	private int noofworkingdays=120;
	
	public abstract double getNetSalary();
	
	public void getDisplay() {
		System.out.println(this.name+" "+this.id+" "+this.salary);
	}
}
class DriveManager extends Emp{
	private String name="rajan";
	private String id="1002";
	private double salary=6000;
	private int noofworkingdays=190;
	
	@Override
	public void getDisplay() {
		System.out.println(this.name+" "+this.id+" "+this.salary);
	}
	@Override
	public double getNetSalary() {
		
		return this.salary*this.noofworkingdays;
	}
}
class Clerk extends Emp{
	private String name="chowdary";
	private String id="1230";
	private double salary=5000;
	private int noofworkingdays=230;
	
	@Override
	public void getDisplay() {
		System.out.println(this.name+" "+this.id+" "+this.salary);
	}
	@Override
	public double getNetSalary() {
		
		return this.salary*this.noofworkingdays;
	}
}