package com.srikanth.oncode.testing;

import java.util.Scanner;

public class InputCharacter {
	

	public static boolean isUpper(String input) {
		for(int i=0;i<input.length();i++) {
			if(!(input.charAt(i)<='Z' && input.charAt(i)>='A')) {
//				System.out.println(input.charAt(i));
				return false;
			}
		}
		return true;
	}
	public static boolean isLower(String input) {
		for(int i=0;i<input.length();i++) {
			if(!(input.charAt(i)<='z' && input.charAt(i)>='a')) {
//				System.out.println(input.charAt(i));
				return false;
			}
		}
		return true;
	}
	public static boolean isNumber(String input) {
		Integer number=Integer.parseInt(input);
		if(number>0 || number<0 || number==0) {
			return true;
		}
		return false;
	}
}
