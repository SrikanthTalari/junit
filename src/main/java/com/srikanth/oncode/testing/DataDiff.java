package com.srikanth.oncode.testing;

import java.time.LocalDate;
import java.time.Period;

public class DataDiff {

	public static Period  diff(LocalDate d1,LocalDate d2) {
		return Period.between(d1, d2);
	}
//	public static void main(String[] args) {
//		Period p=DataDiff.diff(LocalDate.of(2007, 12, 03),LocalDate.of(2007, 12, 03));
//		System.out.println(p);
//	}
}
